const path = require('path')
//define entry point entry -> output
// https://webpack.js.org/concepts/entry-points/
//console.log(path.join(__dirname, 'public'))

module.exports = {
    mode: 'development',
    entry: './src/app.js',
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        }, 
        {            
            test:/\.s?css$/, //This is to target all files ending with .css
            use: [ // this property allow us to load  an array of loaders
                'style-loader',
                'css-loader',
                'sass-loader'
            ]
        }], 
    },
    devtool: 'eval-cheap-module-source-map',
    devServer: {
        static: './public',
        historyApiFallback: true //This is to handle 404 page to redirect to index page
    }
}